﻿MoBlog1.4
作为MoAspEnginer的示例程序，只包含基本的博客功能

一、基本环境需求

本程序调试环境为Windows 2003+IIS6.0以及Windows 7+IIS7.5，IIS6及以上版本均可以正常运行；
请勿使用非IIS的web服务器调试，例如NetBox等；
IIS5调试程序会出现问题：http://www.9fn.net/post/51.html

二、帮助

通过浏览器访问本目录下的install.asp进行安装。例如，http://您的域名/install.asp
安装完成后，请删除或转移install.asp文件以及Install文件夹。

帮助文档地址：http://www.9fn.net/help/

更新日志：http://www.9fn.net/update.html

三、声明

1、本程序作为MoAspEnginer的示例程序而作，程序细节部分未完全考虑，例如后台表单数据的验证等;前台页面的数据验证已做详细处理
2、本程序默认运行在根目录下，如需运行在子目录，请修改404.asp的配置项，例如，如果要运行在目录moblog下，则配置项如下

MO_APP_NAME = "App"	'随意的英文名字，建议使用字母和数字的组合
MO_ROOT = "/moblog/"	'default.asp所在的目录，设置为空将自动获取
MO_CORE = "/moblog/Mo/"	'核心所在的路径，设置为空将自动获取（这个非必要，多个APP可以使用同一个核心）
MO_APP = "/moblog/App/"	'你的当前App所在的路径，设置为空将根据MO_APP_NAME获取
MO_APP_ENTRY = ""	'可自定义入口文件，设置为空将自动获取

3、核心变量必须是Mo，请勿修改为其他名字，即“Dim Mo : Set Mo = New MoAspEnginer : Mo.Run() : Set Mo = Nothing”里面的Mo是固定的，不可修改


四、联系交流
QQ群：127430216-MoBlog交流(MoAspEnginer)