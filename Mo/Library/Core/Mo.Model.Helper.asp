<script language="jscript" runat="server">
var ModelHelper={
	GetConnectionString:function(){
		var connectionstring = "";
		if(this.dbConf["DB_Type"]=="OTHER" || (this.dbConf.hasOwnProperty("DB_Connectionstring") && this.dbConf["DB_Connectionstring"]!="")){
			connectionstring = F.format(this.dbConf["DB_Connectionstring"],this.dbConf["DB_Server"],this.dbConf["DB_Username"],this.dbConf["DB_Password"],this.dbConf["DB_Name"],F.mappath(this.dbConf["DB_Path"]));
		}else if(this.dbConf["DB_Type"]=="ACCESS"){
			connectionstring = "provider=microsoft.jet.oledb.4.0; data source=" + F.mappath(this.dbConf["DB_Path"]);
		}else if(this.dbConf["DB_Type"]=="MSSQL"){
			connectionstring = F.format("provider=sqloledb.1;Persist Security Info=false;data source={0};User ID={1};pwd={2};Initial Catalog={3}",this.dbConf["DB_Server"],this.dbConf["DB_Username"],this.dbConf["DB_Password"],this.dbConf["DB_Name"]);
		}else if(this.dbConf["DB_Type"]=="SQLITE"){
			connectionstring = "DRIVER={SQLite3 ODBC Driver};Database=" + F.mappath(this.dbConf["DB_Path"]);
		}else if(this.dbConf["DB_Type"]=="MYSQL"){
			connectionstring = F.format("DRIVER={mysql odbc 3.51 driver};SERVER={0};USER={1};PASSWORD={2}",this.dbConf["DB_Server"],this.dbConf["DB_Username"],this.dbConf["DB_Password"],this.dbConf["DB_Name"]);
		}
		return connectionstring;
	},
	GetSqls:function(){
		var where_="",order_="",where2_="",groupby="",join="",on="",cname="";
		if(this.strwhere!=""){
			where_=" where " + this.strwhere + "";
			if(this.strpage>1 && this.strlimit!=-1)where2_=" and (" + this.strwhere + ")";
		}
		if(this.strgroupby!="") groupby=" group by " + this.strgroupby;
		if(this.strjoin!="")join=" " + this.strjoin + " ";
		if(this.strcname!="")cname = " " + this.strcname+" ";
		if (this.strorderby!="") order_=" order by " + this.strorderby;
		if(this.pagekeyorder=="" || this.strlimit==-1){
			this.sql="select " + this.fields + " from " + this.joinlevel + this.table + cname + join + where_ + groupby+ order_;
			if(this.dbConf["DB_Type"]=="MYSQL")this.countsql = "select count(" + (this.strcname==""?this.table:this.strcname) + "." + this.pk + ") from " + this.joinlevel + this.table + cname + join + where_ + groupby;
		}else{
			this.countsql = "select count(" + (this.strcname==""?this.table:this.strcname) + "." + this.pk + ") from " + this.joinlevel + this.table + cname + join + where_ + groupby;
			this.sql = ModelHelper.GetSqlByTypes.apply(this,[cname,join,on,where_,groupby,order_]);
		}
	},
	GetSqlByTypes:function(cname,join,on,where_,groupby,order_){
		if(this.dbConf.DB_Type=="MSSQL")return ModelHelper.GetSqlsForMSSQL.apply(this,arguments);
		if(this.dbConf.DB_Type=="MYSQL")return ModelHelper.GetSqlsForMysql.apply(this,arguments);
		if(this.dbConf.DB_Type=="SQLITE")return ModelHelper.GetSqlsForMysql.apply(this,arguments);
		return ModelHelper.GetSqlsForAccess.apply(this,arguments);
	},
	GetSqlsForAccess:function(cname,join,on,where_,groupby,order_){
		var sql;
		if(this.isonlypkorder && this.ballowOnlyPKOrder){
			if(this.strpage>1){
				var c="<",d="min";
				if(this.onlypkorder.toLowerCase()=="asc") {c=">";d="max";}
				where_ +=" " + (where_!=""?"and":"where") + " " + (this.strcname==""?this.table:this.strcname) + "." + this.pagekey + c + " (select " + d + "(" + this.pagekey + ") from (select top " + this.strlimit * (this.strpage-1) + " " + (this.strcname==""?this.table:this.strcname) + "." + this.pagekey + " from " +this.joinlevel + this.table + cname + join + where_ + groupby+ order_ +") as mo_p_tmp)";
			}
			sql="select top " + this.strlimit + " " + this.fields + " from " + this.joinlevel + this.table + cname + join + where_ + groupby+ order_;
		}else{
			if(this.strpage>1)where_ +=" " + (where_!=""?"and":"where") + " " + (this.strcname==""?this.table:this.strcname) + "." + this.pagekey + " not in(select top " + this.strlimit * (this.strpage-1) + " " + (this.strcname==""?this.table:this.strcname) + "." + this.pagekey + " from " +this.joinlevel + this.table + cname + join + where_ + groupby+ order_ +")"	;
			sql="select top " + this.strlimit + " " + this.fields + " from " + this.joinlevel + this.table + cname + join + where_ + groupby+ order_;		
		}
		return sql;
	},
	GetSqlsForMSSQL:function(cname,join,on,where_,groupby,order_){
		if(this.dbConf.DB_Version==2012){
			return "select " + this.fields + " from " + this.joinlevel + this.table + cname + join + where_ + groupby+ order_ +" OFFSET " + (this.strlimit * (this.strpage-1) +1) + " ROWS FETCH NEXT " + this.strlimit + " ROWS ONLY";
		}
		else if(this.dbConf.DB_Version>=2005){
			return "select * from (select " + this.fields + ",ROW_NUMBER() OVER (" + order_ + ") AS ROWID_ from " + this.joinlevel + this.table + cname + join + where_ + groupby +") AS tmp_table_1 where ROWID_ BETWEEN " + (this.strlimit * (this.strpage-1) +1) + " and " + (this.strlimit * this.strpage);
		}
		var sql;
		if(this.isonlypkorder && this.ballowOnlyPKOrder){
			if(this.strpage>1){
				var c="<",d="min";
				if(this.onlypkorder.toLowerCase()=="asc") {c=">";d="max";}
				where_ +=" " + (where_!=""?"and":"where") + " " + (this.strcname==""?this.table:this.strcname) + "." + this.pagekey + c + " (select " + d + "(" + this.pagekey + ") from (select top " + this.strlimit * (this.strpage-1) + " " + (this.strcname==""?this.table:this.strcname) + "." + this.pagekey + " from " +this.joinlevel + this.table + cname + join + where_ + groupby+ order_ +") as mo_p_tmp)";
			}
			sql="select top " + this.strlimit + " " + this.fields + " from " + this.joinlevel + this.table + cname + join + where_ + groupby+ order_;
		}else{
			if(this.strpage>1)where_ +=" " + (where_!=""?"and":"where") + " " + (this.strcname==""?this.table:this.strcname) + "." + this.pagekey + " not in(select top " + this.strlimit * (this.strpage-1) + " " + (this.strcname==""?this.table:this.strcname) + "." + this.pagekey + " from " +this.joinlevel + this.table + cname + join + where_ + groupby+ order_ +")"	;
			sql="select top " + this.strlimit + " " + this.fields + " from " + this.joinlevel + this.table + cname + join + where_ + groupby+ order_;		
		}
		return sql;
	},
	GetSqlsForMysql:function(cname,join,on,where_,groupby,order_){
		if(this.isonlypkorder && this.ballowOnlyPKOrder){
			if(this.strpage>1){
				var c="<=";
				if(this.onlypkorder.toLowerCase()=="asc") c=">=";
				sql="select " + this.fields + " from " + this.joinlevel + this.table + cname + join + where_ + (where_!=""?" and ":" where ") + this.table +"." + this.pk + c
				+ "(select " + (this.strcname==""?this.table:this.strcname) +"." + this.pk + " from " + this.joinlevel + this.table + join + where_+ groupby+ order_ + " limit " + this.strlimit * (this.strpage-1) + ",1)"
				+ groupby+ order_ +" limit " + this.strlimit + "";
			}else{
				sql="select " + this.fields + " from " + this.joinlevel + this.table + cname + join + where_ + groupby+ order_ +" limit 0," + this.strlimit + "";
			}
		}else{
			sql="select " + this.fields + " from " + this.joinlevel + this.table + cname + join + where_ + groupby+ order_ +" limit " + this.strlimit * (this.strpage-1) + "," + this.strlimit + "";
		}
		return sql;
	}
};
function ModelCMDManager(cmd,model){
	this.cmd = cmd ||"";
	this.model = model || null;
	this.parms_={};
	this.cmdobj=F.activex("ADODB.Command");
	this.cmdobj.CommandType=4;     
    this.cmdobj.Prepared = true;
    this.withQuery=true;
    this.parmsGet=false;
    this.totalRecordsParm="";
}
ModelCMDManager.New = function(cmd){return new ModelCMDManager(cmd);};
ModelCMDManager.prototype.addInput = function(name,value,t,size){
	this.parms_[name]={"datatype":t,"size":size,"value":value||null,"type":1};
}
ModelCMDManager.prototype.addInputInt = function(name,value){
	this.addInput(name,3,4,value);
}
ModelCMDManager.prototype.addInputBigInt = function(name,value){
	this.addInput(name,20,8,value);
}
ModelCMDManager.prototype.addInputVarchar = function(name,value,size){
	this.addInput(name,200,size||50,value);
}
ModelCMDManager.prototype.addOutput = function(name,t,size){
	this.parms_[name]={"datatype":t,"size":size,"value":null,"type":2};
}
ModelCMDManager.prototype.addReturn = function(name,t,size){
	this.parms_[name]={"datatype":t,"size":size,"value":null,"type":4};
}
ModelCMDManager.prototype.getparm = function(name){
	if(!this.parmsGet){
		for(var i in this.parms_){
			if(!this.parms_.hasOwnProperty(i))continue;
			if(this.parms_[i]["type"]>1){
				this.parms_[i].value = this.cmdobj(i).value;
			}
		}
		this.parmsGet=true;
	}
	if(!this.parms_.hasOwnProperty(name)) return null;
	return this.parms_[name];
}
ModelCMDManager.prototype.execute = function(){
	this.model.exec(this);
	return this.model;
};
ModelCMDManager.prototype.assign = function(name,asobject){
	return this.model.assign(name,asobject);
};
ModelCMDManager.prototype.exec = function(conn){
	this.cmdobj.ActiveConnection=conn;
	this.cmdobj.CommandText = this.cmd;
	for(var i in this.parms_){
		if(!this.parms_.hasOwnProperty(i))continue;
		if(this.parms_[i].type==2 || this.parms_[i].type==4){
			this.cmdobj.Parameters.Append(this.cmdobj.CreateParameter(i,this.parms_[i].datatype,this.parms_[i]["type"],this.parms_[i].size));
		}else{
			this.cmdobj.Parameters.Append(this.cmdobj.CreateParameter(i,this.parms_[i].datatype,this.parms_[i]["type"],this.parms_[i].size,this.parms_[i].value));
		}
	}
	if(this.withQuery){
		return this.cmdobj.Execute();
	}else{
		this.cmdobj.Execute();
	}
}
function IAction(){
	Mo.assign("MO_METHOD",MO_METHOD);
	Mo.assign("MO_ACTION",MO_ACTION);
}
IAction.prototype.assign = function(key,value){Mo.assign(key,value);};
IAction.prototype.display = function(tpl){Mo.display(tpl);};
</script>
