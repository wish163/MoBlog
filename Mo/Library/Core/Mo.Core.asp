﻿<!--#include file="Mo.Function.Jscript.asp"-->
<!--#include file="Mo.Function.asp"-->
<!--#include file="Mo.List.asp"-->
<!--#include file="Mo.Model.asp"-->
<!--#include file="Mo.Record.asp"-->
<!--#include file="Mo.View.asp"-->
<!--#include file="Mo.MD5.asp"-->
<%
class MoAspEnginer
	private mvarDicts, mvarUse,mvarConfig, mvarAction, mvarMethod,mvarCacheFileName,mvarRealAction,mvarRealMethod
	private mvarLibrarys,mvarStatus,mvarException

	'****************************************************
	'@DESCRIPTION:	set current action,it will be fetched automatic
	'@PARAM:	value [String] : action value
	'****************************************************
	public Property Let Action(byval value)
		mvarAction = value
		mvarRealAction = value
	end Property

	'****************************************************
	'@DESCRIPTION:	get current action
	'@RETURN:	[String] action value
	'****************************************************
	public Property Get Action()
		Action = mvarAction
	end Property

	'****************************************************
	'@DESCRIPTION:	set current method,it will be fetched automatic
	'@PARAM:	value [String] : action value
	'****************************************************
	public Property Let Method(byval value)
		mvarMethod = value
		mvarRealMethod = value
	end Property

	'****************************************************
	'@DESCRIPTION:	get current method
	'@RETURN:	[String] action value
	'****************************************************
	public Property Get Method()
		Method = mvarMethod
	end Property

	'****************************************************
	'@DESCRIPTION:	get enginer version
	'@RETURN:	[String]
	'****************************************************
	public Property Get Version()
		Version = "MoAspEnginer 1.2.5"
	end Property

	'****************************************************
	'@DESCRIPTION:	set HTTP Status
	'@PARAM:	value [String] : HTTP status,Eg. 404,500
	'****************************************************
	public Property Let Status(byval value)
		mvarStatus = value & ""
		if mvarStatus="404" then mvarStatus = "404 Not Found"
		if mvarStatus="200" then mvarStatus = "200 OK"
		if mvarStatus="301" then mvarStatus = "301 Moved Permanently"
		if mvarStatus="500" then mvarStatus = "500 Internal Server Error"
	end Property

	'****************************************************
	'@DESCRIPTION:	get HTTP Status
	'@RETURN:	[String] HTTP Status
	'****************************************************
	public Property Get Status()
		Status = mvarStatus
	end Property

	'****************************************************
	'@DESCRIPTION:	Class_Initialize
	'****************************************************
	private sub Class_Initialize()
		mvarException=null
		mvarStatus = "200 OK"
		if MO_APP_NAME="" then F.exit "未定义应用名称：MO_APP_NAME。"
		if MO_ROOT="" then
			MO_ROOT = F.server("URL")
			MO_ROOT = mid(MO_ROOT,1,instrrev(MO_ROOT,"/"))
		end if
		if MO_APP="" then MO_APP = MO_ROOT & MO_APP_NAME & "/"
		if MO_CORE="" then MO_CORE = MO_ROOT & "Mo/"
		if right(MO_APP,1)<>"/" then MO_APP = MO_APP & "/"
		if right(MO_CORE,1)<>"/" then MO_CORE = MO_CORE & "/"
		if MO_APP_ENTRY="" then 
			MO_APP_ENTRY = mid(F.server("URL"),instrrev(F.server("URL"),"/")+1)
			if lcase(MO_APP_ENTRY)="default.asp" then MO_APP_ENTRY=""
		end if
		set mvarDicts = F.activex("Scripting.Dictionary")
		set mvarUse = F.activex("Scripting.Dictionary")
		set mvarConfig = F.activex("Scripting.Dictionary")
		set mvarLibrarys = F.activex("Scripting.Dictionary")
		if not F.fso.FolderExists(F.mappath(MO_CORE)) then F.exit "核心目录[" & MO_CORE & "]不存在"
		if not MO_DEBUG then on error resume next
		err.clear
		dim file
		for each file in F.fso.getfolder(F.mappath(MO_CORE & "Library/Common")).files
			if right(lcase(file.name),4)=".asp" then Include file.path
		next
		if not F.fso.FolderExists(F.mappath(MO_APP)) then
			F.fso.CreateFolder F.mappath(MO_APP)
			F.fso.CreateFolder F.mappath(MO_APP & "Action")
			F.fso.CreateFolder F.mappath(MO_APP & "Cache")
			F.fso.CreateFolder F.mappath(MO_APP & "Cache/Compiled")
			F.fso.CreateFolder F.mappath(MO_APP & "Cache/Model")
			F.fso.CreateFolder F.mappath(MO_APP & "Templates")
			F.fso.CreateFolder F.mappath(MO_APP & "Config")
			F.fso.CreateFolder F.mappath(MO_APP & "Library")
			F.fso.CreateFolder F.mappath(MO_APP & "Library/Extend")
			F.fso.CreateFolder F.mappath(MO_APP & "Library/TagLib")
			F.fso.CreateFolder F.mappath(MO_APP & "Library/PreLib")
			F.fso.CreateFolder F.mappath(MO_APP & "Library/EndLib")
			F.fso.CreateFolder F.mappath(MO_APP & "Library/Common")
			F.fso.CopyFile F.mappath(MO_CORE & "Action/Action.Home.asp") ,F.mappath(MO_APP & "Action/Action.Home.asp")
		end if
		if F.fso.FileExists(F.mappath(MO_APP & "Config/Config.asp")) then Include F.mappath(MO_APP & "Config/Config.asp")
		if F.fso.FileExists(F.mappath(MO_APP & "Library/Common/Function.asp")) then Include F.mappath(MO_APP & "Library/Common/Function.asp")
		if MO_IMPORT_COMMON_FILES<>"" then
			dim files : files = Split(MO_IMPORT_COMMON_FILES,";")
			if ubound(files)<0 then exit sub
			for each file in files
				if trim(file)<>"" then
					if Instr(file,"=")>0 then
						IncludeFile F.mappath(MO_APP & "Library/Common/" & Mid(file,1,Instr(file,"=")-1) & ".asp"),Mid(file,Instr(file,"=")+1)
					else
						Include F.mappath(MO_APP & "Library/Common/" & file & ".asp")
					end if
				end if
			next
		end if
	end sub

	'****************************************************
	'@DESCRIPTION:	Class_Terminate
	'****************************************************
	private sub Class_Terminate()
		dim d
		for each d in mvarDicts
			if isobject(mvarDicts(d)) then set mvarDicts(d)=nothing
		next
		for each d in mvarUse
			if isobject(mvarUse(d)) then set mvarUse(d)=nothing
		next
		for each d in mvarConfig
			if isobject(mvarConfig(d)) then set mvarConfig(d)=nothing
		next
		set mvarDicts = nothing
		set mvarUse = nothing
		set mvarConfig = nothing
		set mvarLibrarys = nothing
	end sub

	'****************************************************
	'@DESCRIPTION:	include libiary as static object. eg: Static("JsonParser"),Static("TagLib:Tag.Rewrite")
	'@PARAM:	lib [Variant] : library name,Eg. JsonParser. All the Library in Library Folder can be included
	'@RETURN:	[Object] the static object
	'****************************************************
	public function [Static](byval lib)
		if not mvarUse.exists(lib) then set mvarUse(lib) = MoAspEnginer__(lib)
		set [Static] = mvarUse(lib)
	end function

	'****************************************************
	'@DESCRIPTION:	include libiary. eg: MoAspEnginer__("JsonParser"),MoAspEnginer__("TagLib:Tag.Rewrite")
	'@PARAM:	lib [String] : library name,Eg. JsonParser. All the Library in Library Folder can be included
	'@RETURN:	[Object] library included
	'****************************************************
	public default function MoAspEnginer__(byval lib)
		dim useResult:useResult = Use(lib)
		set MoAspEnginer__ = LoadLibrary("",useResult(0),useResult(1))
	end function

	'****************************************************
	'@DESCRIPTION:	include library. eg: Use("JsonParser"),Use("TagLib:Tag.Rewrite")
	'@PARAM:	lib [String] : library name,Eg. JsonParser. All the Library in Library Folder can be included.
	'@RETURN:	[Array] library information. [0]=library class,[1] = library name, [2] = library folder
	'****************************************************
	public function Use(byval lib)
		dim path,core,cls,library
		core="Extend"
		cls = lib
		library = "Lib"
		if instr(lib,":")>0 then
			core = mid(lib,1,instr(lib,":")-1)
			cls = mid(lib,instr(lib,":")+1)
		end if
		if instr(cls,".")>0 then
			library = mid(cls,1,instr(cls,".")-1)
			cls = mid(cls,instr(cls,".")+1)
		end if
		if not mvarLibrarys.exists(library & "_" & cls) then
			path = F.mappath(MO_APP & "Library/" & core & "/Mo." & library & "." & cls & ".asp")
			if not F.fso.fileexists(path) then path = F.mappath(MO_CORE & "Library/" & core & "/Mo." & library & "." & cls & ".asp")
			if F.fso.fileexists(path) then LoadLibrary path,library,cls else Err.Raise 1,"Mo.Use(lib)","Mo.Core.asp, 198 : library '" & cls & "' is not exists."
		end if
		Use = Array(library,cls,core)
	end function

	'****************************************************
	'@DESCRIPTION:	if a Model cache is exists.
	'@PARAM:	name [String] : Cache name
	'@RETURN:	[Boolean] if exists return true,or return false
	'****************************************************
	public function ModelCacheExists(byval name)
		if name="" then
			ModelCacheExists = false
			exit function
		end if
		ModelCacheExists = F.fso.FileExists(F.mappath(MO_APP & "Cache/Model/" & name & ".cak"))
	end function 

	'****************************************************
	'@DESCRIPTION:	save Model cache to local file
	'@PARAM:	name [String] : Cache name
	'@PARAM:	content [String] : Cache content
	'@RETURN:	[Boolean] if save successfully return true,or return false
	'****************************************************
	public function ModelCacheSave(byval name,byref content)
		if name="" then
			ModelCacheSave = false
			exit function
		end if
		ModelCacheSave = [Static]("File").writetext(F.mappath(MO_APP & "Cache/Model/" & name & ".cak"),content,MO_CHARSET)
	end function 

	'****************************************************
	'@DESCRIPTION:	load Model cache from local file
	'@PARAM:	name [String] : Cache name
	'@RETURN:	[String] Cache content
	'****************************************************
	public function ModelCacheLoad(byval name)
		if name="" then
			ModelCacheLoad = ""
			exit function
		end if
		ModelCacheLoad = [Static]("File").readtext(F.mappath(MO_APP & "Cache/Model/" & name & ".cak"),MO_CHARSET)
	end function 

	'****************************************************
	'@DESCRIPTION:	delete Model cache
	'@PARAM:	name [String] : Cache name
	'@RETURN:	[Boolean] if delete successfully return true,or return false
	'****************************************************
	public function ModelCacheDelete(byval name)
		if name="" then
			ModelCacheDelete = false
			exit function
		end if
		ModelCacheDelete = [Static]("File").delete(F.mappath(MO_APP & "Cache/Model/" & name & ".cak"))
	end function 

	'****************************************************
	'@DESCRIPTION:	clear Model cache
	'@RETURN:	[Boolean] if clear successfully return true,or return false
	'****************************************************
	public function ModelCacheClear()
		ModelCacheClear = [Static]("Folder").Clear(F.mappath(MO_APP & "Cache/Model"))
	end function 

	'****************************************************
	'@DESCRIPTION:	clear compiled cache
	'@RETURN:	[Boolean] if clear successfully return true,or return false
	'****************************************************
	public function ClearCompiledCache()
		ClearCompiledCache = [Static]("Folder").Clear(F.mappath(MO_APP & "Cache/Compiled"))
	end function

	'****************************************************
	'@DESCRIPTION:	clear library cache
	'@RETURN:	[Int] library count that was cleared
	'****************************************************
	public function ClearLibraryCache()
		ClearLibraryCache = F.cache.clear(MO_APP_NAME & ".lib.")
	end function

	private function LoadLibrary(byval path,byval library,byval cls)
		if path="" then
			if mvarLibrarys(library & "_" & cls) = "jscript" then 
				set LoadLibrary = F.import("Mo" & library & cls)
			elseif mvarLibrarys(library & "_" & cls) = "vbscript" then
				execute "set LoadLibrary = new Mo" & library & cls
			else
				Err.Raise 2,"Mo.LoadLibrary(path, library, cls)","Mo.Core.asp, 302 : 类库[Mo" & library & cls & "]不存在或初始化失败，请检查类库是否存在"
			end if
			exit function
		end if
		if not MO_DEBUG then on error resume next
		err.clear
		dim ret,language,filesum,isnew
		path = F.mappath(path)
		filesum = library & "." & cls
		isnew = false
		if ((Application(MO_APP_NAME & ".lib." & filesum & "_lng")<>"jscript" and Application(MO_APP_NAME & ".lib." & filesum & "_lng")<>"vbscript"))  or (not MO_LIB_CACHE) then
			ret = LoadFile(path,"UTF-8")
			ret = replaceex(ret,"(^(\s+)|(\s+)$)","")
			ret = trim(ret)
			Application.Lock()
			Application(MO_APP_NAME & ".lib."&filesum & "_cache") =ret
			Application.UnLock() 
			isnew = true
		else
			ret = Application(MO_APP_NAME & ".lib."&filesum & "_cache")
		end if
		if regtest(ret,"^<script(.+?)runat=""server""(.*?)>") then
			ret = ReplaceEx(ret,"^<script(.+?)>(\s*)","")
			ret = ReplaceEx(ret,"(\s*)</script>","")
			F.execute ret,"Mo" & library & cls
			language = "jscript"
		else
			if left(ret,2)="<%" then ret = mid(ret,3)
			if right(ret,2)=replace("% >"," ","") then ret = left(ret,len(ret)-2)
			executeglobal ret
			language = "vbscript"
		end if
		if err then
			LoadLibrary = false
		else
			LoadLibrary = true
			mvarLibrarys(library & "_" & cls) = language
			if isnew then
				Application.Lock()
				Application(MO_APP_NAME & ".lib."&filesum & "_lng") =language
				Application.UnLock()
			end if
		end if
		err.clear		
	end function

	'****************************************************
	'@DESCRIPTION:	display data to template and write to remote.you can display template in different model or different template
	'@PARAM:	template [String] : template name
	'****************************************************
	public function display(template)
		Response.Status = mvarStatus
		Response.AddHeader "Content-Type","text/html; charset=" & MO_CHARSET
		Response.Write fetch(template)
	end function

	'****************************************************
	'@DESCRIPTION:	display data to template and return html.you can display template in different model or different template
	'@DESCRIPTION:	also you can save HTML code to local file as static
	'@PARAM:	template [String] : template name
	'@RETURN:	[String] the last html code
	'****************************************************
	public function fetch(byval template)
		dim html,cachename,OldHash,NewHash,usecache,vbscript,cachepath
		usecache =false
		if MO_COMPILE_CACHE then
			cachename = mvarRealMethod & "^" & mvarRealAction & "^" & replace(template,":","^")
			cachepath = F.mappath(MO_APP & "Cache/Compiled/" & cachename & ".asp")
			if F.FSO.FileExists(cachepath) then
				OldHash = F.FSO.GetFile(cachepath).DateLastModified
				usecache = true
				if MO_COMPILE_CACHE_EXPIRED>0 then
					if datediff("s",OldHash,now())>=MO_COMPILE_CACHE_EXPIRED then usecache=false
				end if
				if usecache then vbscript = LoadVBScript(cachepath,MO_CHARSET)
			end if
		end if
		if not usecache then
			html = LoadTemplateEx(template)
			if html="" then
				fetch=""
				exit function
			end if
			'Execute LoadVBScript(F.mappath(MO_CORE & "Library/Core/Mo.View.asp"),"utf-8")
			vbscript = (new MoAspEnginer_View)(html)
			if MO_COMPILE_CACHE then SaveFile cachepath,"<%"&vbscript&"%" &">",MO_CHARSET
		end if
		ExecuteGlobal vbscript
		if not MO_DIRECT_OUTPUT then fetch = Temp___()
		if MO_CACHE=true and MO_CACHE_DIR<>"" and (not MO_DIRECT_OUTPUT) then
			if F.fso.folderexists(F.mappath(MO_CACHE_DIR)) then SaveFile F.mappath(MO_CACHE_DIR & mvarCacheFileName & ".cache"),fetch,MO_CHARSET
		end if
	end function
	
	private function LoadTemplateEx(Byval template)
		Dim tempStr,Match,fn,Matches,tfun,resultstr
		dim templatelist,vpath,path,html,vbscript,templatelist2
		
		templatelist = split(template,":")
		if ubound(templatelist)=0 then
			vpath = MO_TEMPLATE_NAME & "/" & mvarMethod & MO_TEMPLATE_SPLIT & template
		elseif ubound(templatelist)=1 then
			vpath = MO_TEMPLATE_NAME & "/" &replace(template,":",MO_TEMPLATE_SPLIT)
		elseif ubound(templatelist)=2 then
			vpath = templatelist(0) & "/" & templatelist(1) & MO_TEMPLATE_SPLIT & templatelist(2)
		end if
		path = MO_APP & "Templates/" & vpath & "." & MO_TEMPLATE_PERX
		if instr(vpath,"@")>0 then path = MO_ROOT & mid(vpath,instrrev(vpath,"@")+1) & "/Templates/" & mid(vpath,1,instrrev(vpath,"@")-1) & "." & MO_TEMPLATE_PERX
		if not F.fso.FileExists(F.mappath(path)) then path = MO_CORE & "Templates/" & vpath & "." & MO_TEMPLATE_PERX
		path = F.mappath(path)
		if not F.fso.FileExists(path) then exit function
		tempStr = LoadFile(path,MO_CHARSET)
		Set Matches = GetMatch(tempStr,"<include file\=\""(.+?)(\." & MO_TEMPLATE_PERX & ")?\"" />")
		if Matches.count>0 Then
			For Each Match In Matches
				templatelist2 = RightCopy(templatelist,split(Match.subMatches(0),":"))
				tempStr=Replace(tempStr, Match.value, LoadTemplateEx(Join(templatelist2,":")), 1, -1, 0)
			Next
		End If
		LoadTemplateEx = tempStr
	End Function
	private function ParseTemplatePath(Byval template)
		dim templatelist,vpath
		templatelist = split(template,":")
		if ubound(templatelist)=0 then
			vpath = MO_TEMPLATE_NAME & "/" & mvarMethod & MO_TEMPLATE_SPLIT & template
		elseif ubound(templatelist)=1 then
			vpath = MO_TEMPLATE_NAME & "/" &replace(template,":",MO_TEMPLATE_SPLIT)
		elseif ubound(templatelist)=2 then
			vpath = templatelist(0) & "/" & templatelist(1) & MO_TEMPLATE_SPLIT & templatelist(2)
		end if
		ParseTemplatePath = vpath
	end function

	'****************************************************
	'@DESCRIPTION:	to ensure if the template is in app directory
	'@PARAM:	template [Variant] : template name
	'@RETURN:	[Boolean] if exists return true,else return false
	'****************************************************
	public function templateIsInApp(Byval template)
		dim vpath,path
		vpath = ParseTemplatePath(template)
		path = MO_APP & "Templates/" & vpath & "." & MO_TEMPLATE_PERX
		if instr(vpath,"@")>0 then path = MO_ROOT & mid(vpath,instrrev(vpath,"@")+1) & "/Templates/" & mid(vpath,1,instrrev(vpath,"@")-1) & "." & MO_TEMPLATE_PERX
		templateIsInApp = F.fso.FileExists(F.mappath(path))
	End Function
	
	'****************************************************
	'@DESCRIPTION:	to ensure if the template is in core directory
	'@PARAM:	template [Variant] : template name
	'@RETURN:	[Boolean] if exists return true,else return false
	'****************************************************
	public function templateIsInCore(Byval template)
		dim vpath,path
		vpath = ParseTemplatePath(template)
		path = MO_CORE & "Templates/" & vpath & "." & MO_TEMPLATE_PERX
		templateIsInCore = F.fso.FileExists(F.mappath(path))
	End Function
	
	'****************************************************
	'@DESCRIPTION:	assign data to system
	'@PARAM:	key [String] : variable name
	'@PARAM:	value [Variant] : variable value. it can be an Jscript object
	'****************************************************
	public function assign(byval key,byref value)
		if typename(value)="JScriptTypeInfo" then
			if MO_COMPILE_STRICT then Execute "set " & key & " = value" else set mvarDicts(key) = value
		else
			if MO_COMPILE_STRICT then Execute key & " = value" else mvarDicts(key) = value
		end if
	end function

	'****************************************************
	'@DESCRIPTION:	if an variable is exists
	'@PARAM:	key [String] : variable name
	'@RETURN:	[Boolean] if exists return true,or return false
	'****************************************************
	public function Exists(byval key)
		Exists = mvarDicts.exists(key)
	end function

	'****************************************************
	'@DESCRIPTION:	get the value of defined variable
	'@PARAM:	key [String] : variable name
	'@RETURN:	[Variant] variable value.
	'****************************************************
	public function Value(byval key)
		if MO_COMPILE_STRICT then 
			dim ty:ty = eval("typename(" & key & ")")
			if ty="JScriptTypeInfo" then
				Execute "Set Value = " & key
			else
				Execute "Value = " & key
			end if
			exit function
		end if
		if not mvarDicts.exists(key) then 
			Value = empty
			exit function
		end if
		if typename(mvarDicts(key))="JScriptTypeInfo" then
			set Value = mvarDicts(key) 
			if Value.isset__("Reset") then Value.Reset()
		else
			Value = mvarDicts(key) 
		end if
	end function

	'****************************************************
	'@DESCRIPTION:	get the property value of an object
	'@PARAM:	l [String] : Object name(variable name)
	'@PARAM:	k [String] : property name
	'@RETURN:	[Variant] property value
	'****************************************************
	public function values(byval l,byval k)
		if MO_COMPILE_STRICT then 
			Execute "values = " & l & "." & k
			exit function
		end if
		if not mvarDicts.exists(l) then
			values = ""
			exit function
		end if 
		if typename(mvarDicts(l))="JScriptTypeInfo" then
			if mvarDicts(l).isset__(k) then
				execute "values = mvarDicts(l)." & k
				exit function
			end if
		end if
		values=""
	end function

	'****************************************************
	'@DESCRIPTION:	load a config. the config files are in 'Config' directory of Core or App.
	'@PARAM:	lib [String] : config name
	'@RETURN:	[Object] config object
	'****************************************************
	public function C(byval lib)
		if mvarConfig.exists(lib) then
			set C = mvarConfig(lib)
			exit function
		end if
		dim filepath : filepath = F.mappath(MO_APP & "Config/Mo.Conf." & lib & ".asp")
		if not F.fso.fileexists(filepath) then filepath = F.mappath(MO_CORE & "Config/Mo.Conf." & lib & ".asp")
		if F.fso.fileexists(filepath) then
			if LoadLibrary(filepath,"Conf",lib) then
				set mvarConfig(lib) = LoadLibrary("","Conf",lib)
				set C = mvarConfig(lib)
			else
				Err.Raise 3,"Mo.C(lib)","Mo.Core.asp, 547 : 配置[" & lib & "]无法加载,请检查配置文件是否正确"
			end if
		else
			Err.Raise 4,"Mo.C(lib)","Mo.Core.asp, 546 : 配置[" & lib & "]无法加载,请检查配置文件是否存在"
		end if
	end function

	'****************************************************
	'@DESCRIPTION:	load another action
	'@PARAM:	lib [String] : action name
	'@RETURN:	[Object] action object
	'****************************************************
	public function A(byval lib)
		if mvarLibrarys("Action" & "_" & lib) = "jscript" then 
			set A = F.import(Action & lib)
			exit function
		elseif mvarLibrarys("Action" & "_" & lib) = "vbscript" then
			execute "set A = new Action" & lib
			exit function
		end if
		dim filepath : filepath = F.mappath(MO_APP & "Action/Action." & lib & ".asp")
		if not F.fso.fileexists(filepath) then filepath = F.mappath(MO_CORE & "Action/Action." & lib & ".asp")
		if F.fso.fileexists(filepath) then
			if LoadModel(filepath,lib) then
				set A = LoadModel("",lib)
			else
				Err.Raise 5,"Mo.A(lib)","Mo.Core.asp, 574 : 模块[" & lib & "]无法加载,请检查模块文件"
			end if
		else
			Err.Raise 6,"Mo.A(lib)","Mo.Core.asp, 573 : 模块[" & lib & "]无法加载,请检查模块文件是否存在"
		end if
	end function

	'****************************************************
	'@DESCRIPTION:	load TagLib to do something for template content. this method will be called automatic.
	'@PARAM:	mvarContent [String] : template content
	'****************************************************
	public sub TagLib(byref mvarContent)
		if MO_TAG_LIB<>"" then
			dim libs,lib,T__
			libs = split(MO_TAG_LIB,",")
			for each lib in libs
				Call MoAspEnginer__("TagLib:Tag." & lib)(mvarContent)
			next
		end if
	end sub
	private sub Start__()
		if MO_PRE_LIB<>"" then
			dim libs,lib,T__
			libs = split(MO_PRE_LIB,",")
			for each lib in libs
				Call MoAspEnginer__("PreLib:Pre." & lib)(Mo.method,Mo.Action)
			next
		end if
	end sub	
	private sub End__()
		if MO_END_LIB<>"" then
			dim libs,lib,T__
			libs = split(MO_END_LIB,",")
			for each lib in libs
				Call MoAspEnginer__("EndLib:End." & lib)(Mo.method,Mo.Action)
			next
		end if
	end sub

	'****************************************************
	'@DESCRIPTION:	Debug something
	'****************************************************
	public sub Debug_()
		if (err and MO_SHOW_SERVER_ERROR) then
			if err.number=438 then
				F.echo "动作[" & mvarAction & "]错误：模块[" & mvarMethod & "]的默认动作" & mvarAction & "未定义或程序使用了未定义的函数",true
			elseif err.number>0 then
				F.echo "系统错误：["  & err.number &" = " & err.source & "]" & err.description,true
			end if
		end if
	end sub

	'****************************************************
	'@DESCRIPTION:	the entry or program
	'****************************************************
	public sub Run()
		Response.Charset=MO_CHARSET
		if not RegTest(Mo_METHOD_CHAR,"^(\w+)$") then Mo_METHOD_CHAR = "m"
		if not RegTest(Mo_ACTION_CHAR,"^(\w+)$") then Mo_ACTION_CHAR = "a"
		mvarMethod = Trim(F.get(Mo_METHOD_CHAR))
		mvarAction = Trim(F.get(Mo_ACTION_CHAR))
		If mvarAction = "" Then mvarAction = "Index"
		If mvarMethod = "" Then mvarMethod = "Home"
		if not RegTest(mvarAction,"^(\w+)$") then mvarAction = "Index"
		if not RegTest(mvarMethod,"^(\w+)$") then mvarMethod = "Home"
		if DISABLED_MODELS<>"" then
			if instr("," & DISABLED_MODELS & ",","," & mvarMethod & ",")>0 then
				F.exit "Mo.Core.asp : 模块[" & mvarMethod & "]已被禁止自动调用。"
			end if
		end if
		mvarCacheFileName = Md5(F.server("URL") & request.QueryString & "")
		if MO_CACHE=true then
			if F.fso.fileexists(F.mappath(MO_CACHE_DIR & mvarCacheFileName & ".cache")) then
				Response.Write LoadFile(F.mappath(MO_CACHE_DIR & mvarCacheFileName & ".cache"),MO_CHARSET)
				exit sub
			end if
		end if
		dim theMethod
		theMethod = mvarMethod
		ModelPath = MO_APP & "Action/Action." & mvarMethod & ".asp"
		if not F.fso.FileExists(F.mappath(ModelPath)) then
			ModelPath = MO_APP & "Action/Action.Empty.asp"
			theMethod ="Empty"
			if not F.fso.FileExists(F.mappath(ModelPath)) then
				ModelPath =MO_CORE & "Action/Action." & mvarMethod & ".asp"
				theMethod =mvarMethod
				if not F.fso.FileExists(F.mappath(ModelPath)) then
					ModelPath = MO_CORE & "Action/Action.Empty.asp"
					theMethod ="Empty"
					if not F.fso.FileExists(F.mappath(ModelPath)) then F.exit "Mo.Core.asp, 663 : 模块[" & mvarMethod & "]不存在"
				end if
			end if
		end if
		mvarRealMethod = theMethod
		mvarRealAction = mvarAction
		
		Execute "MO_METHOD = theMethod"
		Execute "MO_ACTION = mvarAction"
		Call Start__()
		if not LoadModel(F.mappath(ModelPath),theMethod) then F.exit "Mo.Core.asp, 673 : 模块[" & mvarMethod & "]加载失败(" & mvarException.description & ")"
		if not MO_DEBUG then on error resume next
		err.clear
		dim ModelClass:set ModelClass = LoadModel("",theMethod)
		Execute "Call ModelClass." & mvarAction & "()"
		if err.number=438 then
			err.clear
			mvarRealAction = "Empty"
			Execute "Call ModelClass.Empty(""" & mvarAction & """)"
		end if
		Set ModelClass = Nothing
		Call End__()
		Model__.dispose()
		F.dispose()
		Call Debug_()
	end sub
	private function LoadModel(byval path,byval model)
		if path="" then
			if mvarLibrarys("Action" & "_" & model) = "jscript" then 
				set LoadModel = F.import("Action" & model)
			elseif mvarLibrarys("Action" & "_" & model) = "vbscript" then
				execute "set LoadModel = new Action" & model
			end if
			exit function
		end if
		if not MO_DEBUG then on error resume next
		err.clear
		dim ret,language
		ret = LoadFile(F.mappath(path),MO_CHARSET)
		ret = replaceex(ret,"(^(\s+)|(\s+)$)","")
		ret = trim(ret)
		if regtest(ret,"^<script(.+?)runat=""server""(.*?)>") then
			ret = ReplaceEx(ret,"^<script(.+?)>(\s*)","")
			ret = ReplaceEx(ret,"(\s*)</script>","")
			if not F.execute(ret,"Action" & model) then Err.Raise F.exception.Number,"JScript",F.exception.Description
			language = "jscript"
		else
			if left(ret,2)="<%" then ret = mid(ret,3)
			if right(ret,2)=replace("% >"," ","") then ret = left(ret,len(ret)-2)
			executeglobal ret
			language = "vbscript"
		end if
		if err then
			LoadModel = false
			set mvarException = F.json("{""Number"":""" & err.Number & """,""Source"":""" & F.jsEncode(err.Source) & """,""Description"":""" & F.jsEncode(err.Description) & """}")
		else
			LoadModel = true
			mvarLibrarys("Action" & "_" & model) = language
		end if
		err.clear
	end function

	'****************************************************
	'@DESCRIPTION:	get Records Affected count of a sql query
	'@PARAM:	conn [object(Connection)] : an opened connection object
	'@PARAM:	sqlstring [String] : sql string 
	'@RETURN:	[Int] Affected count
	'****************************************************
	public function RecordsAffected(byref conn,byval sqlstring)
		conn.execute sqlstring,RecordsAffected
	end function

	'****************************************************
	'@DESCRIPTION:	output assigned variable
	'****************************************************
	public sub dump()
		F.dump mvarDicts
	end sub
end class
%>