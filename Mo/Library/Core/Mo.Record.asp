<script language="jscript" runat="server">
/****************************************************
'@DESCRIPTION:	create record object. you can use it in vbscript.
'****************************************************/
function Record__(){
	return new __Record__(arguments);	
}

/****************************************************
'@DESCRIPTION:	create record object
'@PARAM:	args [arguments] : init data(arguments length = 2 * x). eg: var record = new __Record__("name","anlige","age",23);
'****************************************************/
function __Record__(args){
	this.table={};
	this.pk="";
	if(args.length % 2==0){
		for(var i=0;i<args.length-1;i+=2){
			this.set(args[i],args[i+1]);
		}
	}
}

/****************************************************
'@DESCRIPTION:	init data from post data
'@PARAM:	pk [String] : primary key of table
'@RETURN:	[Object] return self
'****************************************************/
__Record__.prototype.frompost=function(pk){
	pk = pk ||"";
	F.post();
	for(var i in F.post__){
		if(i.length>2 && i.substr(i.length-2)==":i")continue;
		if(!F.post__.hasOwnProperty(i))continue;
		if(i.toLowerCase()!=pk.toLowerCase()){
			this.set(i,F.post__[i]);
		}else{
			this.pk = F.post__[i];
		}
	}
	return this;
};

/****************************************************
'@DESCRIPTION:	set field value
'@PARAM:	name [String] : field name
'@PARAM:	value [Variant] : field value
'@PARAM:	type [Variant] : field type. it can be blank
'@RETURN:	[Object] return self
'****************************************************/
__Record__.prototype.set=function(name,value,type){
	type=type||"string";
	delete this.table[name];
	this.table[name]={"value":value,"type":type}
	return this;
};

/****************************************************
'@DESCRIPTION:	get field value
'@PARAM:	name [String] : field name
'@RETURN:	[Variant] field value
'****************************************************/
__Record__.prototype.get=function(name){
	if(this.table[name]!==undefined)return this.table[name].value;
	return "";
};

/****************************************************
'@DESCRIPTION:	remove field
'@PARAM:	[names] [arguments] : field name. Eg: remove("name","age")
'@RETURN:	[Object] return self
'****************************************************/
__Record__.prototype.remove=function(){
	for(var i=0;i<arguments.length;i++){
		delete this.table[arguments[i]];
	}
	return this;
};

/****************************************************
'@DESCRIPTION:	clear all fields
'@RETURN:	[Object] return self
'****************************************************/
__Record__.prototype.clear=function(){
	delete this.table;
	this.table={};
	return this;
};

/****************************************************
'@DESCRIPTION:	assign record to system
'@PARAM:	name [String] : variable name
'@RETURN:	[Object] return self
'****************************************************/
__Record__.prototype.assign=function(name){
	var obj = {};
	for(var i in this.table){
		if(!this.table.hasOwnProperty(i))continue;
		obj[i]=	this.table[i].value;
	}
	Mo.assign(name,obj);
	return this;
};
</script>
