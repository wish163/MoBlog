﻿<%
class ActionEmpty
	private sub Class_Initialize()
		MO_TEMPLATE_NAME = Model__("System","id").where("id=1").query().assign("System",true).read("blog_template")
		if F.get.safe("view")="yes" then MO_TEMPLATE_NAME = F.get.safe("name")
		if MO_TEMPLATE_NAME="" then MO_TEMPLATE_NAME="default"
		
		if Mo.Values("System","blog_state_closed") = "yes" then
			Response.Status = "404 Not Found"
			F.exit Mo.Values("System","blog_state_closed_reason")
		end if
	end sub
	private sub common__
		Model__("Tags","id").select("top 50 *").orderby("counts desc").query().assign "TagsCloud" 
		Model__("Diary","id").select("library,count(library) as num").where("Is_Secret=0").groupby("library").orderby("library desc").query().assign "Librarys"
		dim list
		set list = Mo___()
		set D = Model__("Classes","id").where("Class_Type=" & Model__("Classes","id").where("Class_Tag='cats'").query().read("ID")).orderby("Class_Order desc").query().fetch()
		'仅支持二级目录
		dim index,counts,idlist
		do while not D.Eof()
			set D_ = D.Read()
			index = list.addnew()
			list.set "ID",D_.getter__("ID")
			list.set "Class_Name",D_.getter__("Class_Name")
			list.set "Class_PName",""
			list.set "Class_Tag",D_.getter__("Class_Tag")
			counts = D_.getter__("Class_Count")
			idlist = D_.getter__("ID")
			set D__ = Model__("Classes","id").where("Class_Type=" & D_.getter__("ID")).orderby("Class_Order desc").query().fetch()
			do while not D__.eof()
				set D___ = D__.Read()
				list.addnew()
				list.set "ID",D___.getter__("ID")
				list.set "Class_Name",D___.getter__("Class_Name")
				list.set "Class_PName",D_.getter__("Class_Name")
				list.set "Class_Tag",D___.getter__("Class_Tag")
				list.set "counts",D___.getter__("Class_Count")
				list.set "idlist",D___.getter__("ID")
				idlist = idlist & "," & D___.getter__("ID")
				counts = counts + D___.getter__("Class_Count")
			loop
			list.set "counts",counts,index
			list.set "idlist",idlist,index
		loop
		Mo.assign "cats",list
		Model__("Links","id").where("ischecked=1").select("top 20 title,url,author,id").orderby("link_order desc").query().assign "Links"
		Model__("Guestbook","id").select("top 10 id,pid as article_id,content,name").where("forarticle=1 and secret='no' and checked=1").orderby("id desc").query().assign "Comments"
		Model__("Guestbook","id").select("top 10 id,pid,Content,name").where("forarticle=0 and secret='no' and checked=1").orderby("id desc").query().assign "Guests"
		Model__("Album","id").select("top 6 id,name,(select top 1 t_path from " & MO_TABLE_PERX & "Album_Photos where albumid=" & MO_TABLE_PERX & "Album.id order by isfirst desc,id desc) as t_path").where("issecret='n'").orderby("orderby desc,id desc").query().assign "Albums"
	end sub	
	private sub Class_Terminate()
	end sub
	
	public sub [empty](action)
		call common__()
		Response.Status = "404 Not Found"
		Mo.Assign "action",Mo.Action
		Mo.Assign "method",Mo.Method
		Mo.display "Home:Error"
	end sub	
end class
%>