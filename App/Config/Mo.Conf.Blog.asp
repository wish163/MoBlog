﻿<script type="text/javascript" runat="server">
var MoConfBlog = {
	Version:"MoBlog 1.4",
	UploadMaxSize:"10mb",
	UploadAllowFileTypes:"*.bmp;*.rar;*.jpg;*.gif;*.png;*.doc;*.zip;*.pdf;",
	AlbumPath:MO_ROOT + "App-Contents/Album/",
	AttachmentPath:MO_ROOT + "App-Contents/Attachments/",
	ImagesUploadPath:MO_ROOT + "App-Contents/Images/Upload/",
	ThumbMaxWidth:200,
	ThumbMaxHeight:10000
};
</script>