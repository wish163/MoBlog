﻿<%
class ActionHome
	private sub Class_Initialize()
	end sub
	
	private sub Class_Terminate()
	end sub
	
	public sub Index
		Mo.assign "title","欢迎使用MoBlog安装程序！"
		Mo.assign "path",RndStr(10)
		Dim File:Set File = Mo.Static("File")
		Dim Folder:Set Folder = Mo.Static("Folder")
		File.writetext "App/Config/motest.asp","moblog write test"
		if File.exists("App/Config/motest.asp") then Mo.assign "AppConf","yes" else Mo.assign "AppConf","no"
		
		File.writetext "App-Contents/Data/motest.asp","moblog write test"
		if File.exists("App-Contents/Data/motest.asp") then Mo.assign "AppData","yes" else Mo.assign "AppData","no"
		
		File.writetext "App-Contents/Album/motest.asp","moblog write test"
		if File.exists("App-Contents/Album/motest.asp") then Mo.assign "AppAlbum","yes" else Mo.assign "AppAlbum","no"
		
		
		Folder.create "App-Contents/Album/motest/"
		if Folder.exists("App-Contents/Album/motest/") then Mo.assign "AppAlbumCreate","yes" else Mo.assign "AppAlbumCreate","no"
		
		
		File.writetext "App-Contents/Attachments/motest.asp","moblog write test"
		if File.exists("App-Contents/Attachments/motest.asp") then Mo.assign "AppAttachments","yes" else Mo.assign "AppAttachments","no"
		
		File.writetext "App-Contents/Images/Upload/motest.asp","moblog write test"
		if File.exists("App-Contents/Images/Upload/motest.asp") then Mo.assign "AppImages","yes" else Mo.assign "AppImages","no"
		
		File.writetext "Install/Config/motest.asp","moblog write test"
		if File.exists("Install/Config/motest.asp") then Mo.assign "InstallConf","yes" else Mo.assign "InstallConf","no"

		If F.activex_("Scripting.FileSystemObject") Then Mo.assign "FSO","yes" else Mo.assign "FSO","no"
		If F.activex_("Scripting.Dictionary") Then Mo.assign "IDCT","yes" else Mo.assign "IDCT","no"
		If F.activex_("ADODB.STREAM") Then Mo.assign "STREAM","yes" else Mo.assign "STREAM","no"
		If F.activex_("ADODB.CONNECTION") Then Mo.assign "CONNECTION","yes" else Mo.assign "CONNECTION","no"
		If F.activex_("ADODB.RECORDSET") Then Mo.assign "RECORDSET","yes" else Mo.assign "RECORDSET","no"
		If F.activex_("ADOX.Catalog") Then Mo.assign "Catalog","yes" else Mo.assign "Catalog","no"
		If F.activex_("Persits.Jpeg") Then Mo.assign "Jpeg","yes" else Mo.assign "Jpeg","no"
		If F.activex_("JMail.Message") Then Mo.assign "Jmail","yes" else Mo.assign "Jmail","no"
		If F.activex_("JMail.SMTPMail") Then Mo.assign "SMTP","yes" else Mo.assign "SMTP","no"
		If F.activex_("JMail.POP3") Then Mo.assign "POP3","yes" else Mo.assign "POP3","no"
		If F.activex_("Microsoft.XMLDOM") Then Mo.assign "XMLDOM","yes" else Mo.assign "XMLDOM","no"
		
		File.delete "App/Config/motest.asp"
		File.delete "App-Contents/Data/motest.asp"
		File.delete "App-Contents/Album/motest.asp"
		File.delete "App-Contents/Attachments/motest.asp"
		File.delete "App-Contents/Images/Upload/motest.asp"
		File.delete "Install/Config/motest.asp"
		Folder.delete "App-Contents/Album/motest/"
		Mo.display "index"
	end sub
	
	public sub [empty](action)
		Response.Status = "404 Not Found"
		F.echo "404 Not Found"
	end sub
	
	public sub deleteinstall
		Dim File:Set File = Mo.Static("File")
		Dim Folder:Set Folder = Mo.Static("Folder")
		File.delete "install.asp"
		Folder.delete "install"
	end sub
	'保存管理员信息
	public sub docomplete
		if F.post.exp("adm_username","^(\w{1,20})$")="" then
			F.echo "管理员用户名不能为空且少于20个字符，支持数字、字母以及下划线"
		elseif F.post("adm_password")="" then
			F.echo "管理员密码不能为空"
		elseif F.post("adm_password")<>F.post("adm_repassword") then
			F.echo "两次密码输入不一致"
		else
			rndstr_ = Rndstr1(10)
			Model__("Admin","id").update "An_Admin",F.post("adm_username"),"An_Pwd",Md5(F.post("adm_password") & rndstr_),"An_Rnd",rndstr_
			F.echo "管理员设置成功"
		end if
	end sub
	public sub dbconnecttest
		if f.post.Safe("server")="" then
			f.echo "请填写数据库服务器"
			ef = true
		elseif f.post.Safe("username")="" then
			f.echo "请填写数据库登录名"
			ef = true
		elseif f.post.Safe("password")="" then
			f.echo "请填写数据库登录密码"
			ef = true
		elseif f.post.Safe("dbname")="" then
			f.echo "请填写数据库名"
			ef = true
		else
			Mo.Use "ADOX"
			Set ADOX = MoLibADOX.MSSQL()
			if not ADOX.Open(f.post.Safe("server"),f.post.Safe("username"),f.post.Safe("password"),f.post.Safe("dbname")) then
				f.echo "<span class=""red"">数据库连接失败。" & ADOX.debug() & "</span>"
				ef = true
			else
				f.echo "数据库连接成功。"
			end if
		end if	
	end sub
	public sub docreatetable
		dim ec,ef
		ef = false
		if F.post.exp("adm_username","^(\w{1,20})$")="" then
			ec = ec & "管理员用户名不能为空且少于20个字符，支持数字、字母以及下划线"
			ef = true
		elseif F.post("adm_password")="" then
			ec = ec & "管理员密码不能为空"
			ef = true
		elseif F.post("adm_password")<>F.post("adm_repassword") then
			ec = ec & "两次密码输入不一致"
			ef = true
		elseif f.post.exp("dbtype","^(access|mssql)$")="" then
			ec = ec & "请选择数据库类型"
			ef = true
		elseif f.post.exp("prex","^(\w+)$")="" then
			ec = ec & "请填写数据表后缀"
			ef = true
		else
			dim ADOX,prex
			prex = f.post("prex")
			Mo.Use "ADOX"
			if f.post("dbtype")="access" then
				if f.post.Safe("path")="" then
					ec = ec & "请填写数据库路径"
					ef = true
				else
					Set ADOX = MoLibADOX.ACCESS(f.post.Safe("path"))
					if not ADOX.create() then
						ec = ec & "数据库创建失败：" & ADOX.debug() & ""
						ef = true
					else
						if not ADOX.Open() then
							ec = ec & "<span class=""red"">数据库打开失败。" & ADOX.debug() & "</span>"
							ef = true
						end if
					end if
				end if
			else
				if f.post.Safe("server")="" then
					ec = ec & "请填写数据库服务器"
					ef = true
				elseif f.post.Safe("username")="" then
					ec = ec & "请填写数据库登录名"
					ef = true
				elseif f.post.Safe("password")="" then
					ec = ec & "请填写数据库登录密码"
					ef = true
				elseif f.post.Safe("dbname")="" then
					ec = ec & "请填写数据库名"
					ef = true
				else
					Set ADOX = MoLibADOX.MSSQL()
					if not ADOX.Open(f.post.Safe("server"),f.post.Safe("username"),f.post.Safe("password"),f.post.Safe("dbname")) then
						ec = ec & "<span class=""red"">数据库连接失败。" & ADOX.debug() & "</span>"
						ef = true
					else
						prex = "dbo." & prex
					end if
				end if
			end if
			if not ef then
				Set Fields = ADOX.CreateFieldsCollection()
				Fields.Append ADOX.CreateField("id").int().primarykey().IDENTITY(true)
				Fields.Append ADOX.CreateField("An_Admin").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("An_Pwd").VARCHAR().length(100).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("An_LastDate").datetime().nullable(true)
				Fields.Append ADOX.CreateField("An_LastIP").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("An_LogCount").int(0).nullable(true)
				Fields.Append ADOX.CreateField("An_Rnd").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				if ADOX.CreateTable("" & prex & "Admin",Fields,true) then
					ec = ec & "数据表[Admin]创建成功!<br />"
					dim rndstr_:rndstr_ = Rndstr1(10)
					ADOX.Exec "insert into " & prex & "Admin(An_Admin,An_Pwd,An_LogCount,An_Rnd) values('" & F.post("adm_username") & "','" & Md5(F.post("adm_password") & rndstr_) & "',0,'" & rndstr_ & "')"
				else
					ec = ec & "<span class=""red"">数据表[Admin]创建失败!</span><br />"
					ef = true
				end if
			end if
			if not ef then	
				'创建表：Album
				Set Fields = ADOX.CreateFieldsCollection()
				Fields.Append ADOX.CreateField("id").int().primarykey().IDENTITY(true)
				Fields.Append ADOX.CreateField("name").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("orderby").int(0).nullable(true)
				Fields.Append ADOX.CreateField("description").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("address").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("create_date").datetime().nullable(true)
				Fields.Append ADOX.CreateField("photo_date").datetime().nullable(true)
				Fields.Append ADOX.CreateField("tag").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("issecret").VARCHAR().length(10).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				if ADOX.CreateTable("" & prex & "Album",Fields,true) then
					ec = ec & "数据表[Album]创建成功!<br />"
				else
					ec = ec & "<span class=""red"">数据表[Album]创建失败!</span><br />"
					ef = true
				end if
			end if
			if not ef then		
				'创建表：Album_Photos
				Set Fields = ADOX.CreateFieldsCollection()
				Fields.Append ADOX.CreateField("id").int().primarykey().IDENTITY(true)
				Fields.Append ADOX.CreateField("name").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("description").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("create_date").datetime().nullable(true)
				Fields.Append ADOX.CreateField("photo_date").datetime().nullable(true)
				Fields.Append ADOX.CreateField("isfirst").int(0).nullable(true)
				Fields.Append ADOX.CreateField("width").int(0).nullable(true)
				Fields.Append ADOX.CreateField("height").int(0).nullable(true)
				Fields.Append ADOX.CreateField("path").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("t_path").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("albumid").int(0).nullable(true)
				Fields.Append ADOX.CreateField("orderby").int(0).nullable(true)
				Fields.Append ADOX.CreateField("t_width").int(0).nullable(true)
				Fields.Append ADOX.CreateField("t_height").int(0).nullable(true)
				if ADOX.CreateTable("" & prex & "Album_Photos",Fields,true) then
					ec = ec & "数据表[Album_Photos]创建成功!<br />"
				else
					ec = ec & "<span class=""red"">数据表[Album_Photos]创建失败!</span><br />"
					ef = true
				end if
			end if
			if not ef then		
				'创建表：Attachment
				Set Fields = ADOX.CreateFieldsCollection()
				Fields.Append ADOX.CreateField("id").int().primarykey().IDENTITY(true)
				Fields.Append ADOX.CreateField("name").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("path").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("upload_date").datetime("getdate()").nullable(true)
				Fields.Append ADOX.CreateField("update_date").datetime("getdate()").nullable(true)
				Fields.Append ADOX.CreateField("counts").int(0).nullable(true)
				if ADOX.CreateTable("" & prex & "Attachment",Fields,true) then
					ec = ec & "数据表[Attachment]创建成功!<br />"
				else
					ec = ec & "<span class=""red"">数据表[Attachment]创建失败!</span><br />"
					ef = true
				end if
			end if
			if not ef then
				'创建表：Classes
				Set Fields = ADOX.CreateFieldsCollection()
				Fields.Append ADOX.CreateField("ID").int().primarykey().IDENTITY(true)
				Fields.Append ADOX.CreateField("Class_Name").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("Class_Type").int(0).nullable(true)
				Fields.Append ADOX.CreateField("Class_Order").int(0).nullable(true)
				Fields.Append ADOX.CreateField("Class_Tag").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("Class_Count").int(0).nullable(true)
				Fields.Append ADOX.CreateField("Class_TotalCount").int(0).nullable(true)
				if ADOX.CreateTable("" & prex & "Classes",Fields,true) then
					ec = ec & "数据表[Classes]创建成功!<br />"
					ADOX.Exec "insert into " & prex & "Classes(Class_Name,Class_Type,Class_Order,Class_Tag,Class_Count,Class_TotalCount) values('博客文章',0,2,'cats',0,0)"
					ADOX.Exec "insert into " & prex & "Classes(Class_Name,Class_Type,Class_Order,Class_Tag,Class_Count,Class_TotalCount) values('首次使用',1,1,'first',1,1)"
				else
					ec = ec & "<span class=""red"">数据表[Classes]创建失败!</span><br />"
					ef = true
				end if
			end if
			if not ef then		
				'创建表：Diary
				Set Fields = ADOX.CreateFieldsCollection()
				Fields.Append ADOX.CreateField("id").int().primarykey().IDENTITY(true)
				Fields.Append ADOX.CreateField("Title").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("Content").text().nullable(true)
				Fields.Append ADOX.CreateField("AddDate").datetime("getdate()").nullable(true)
				Fields.Append ADOX.CreateField("Actor").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("FromU").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("ClassID").int(0).nullable(true)
				Fields.Append ADOX.CreateField("Is_Secret").int(0).nullable(true)
				Fields.Append ADOX.CreateField("Is_Self").int(0).nullable(true)
				Fields.Append ADOX.CreateField("views").int(0).nullable(true)
				Fields.Append ADOX.CreateField("Tags").VARCHAR().length(100).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("Is_Top").int(0).nullable(true)
				Fields.Append ADOX.CreateField("can_comment").int(1).nullable(true)
				Fields.Append ADOX.CreateField("library").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("ClassName").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("ClassPath").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("trackback").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("trackback_count").int(0).nullable(true)
				if ADOX.CreateTable("" & prex & "Diary",Fields,true) then
					ec = ec & "数据表[Diary]创建成功!<br />"
					ADOX.Exec "insert into " & prex & "Diary(Title,Content,FromU,ClassID,Tags,library,ClassName,ClassPath) values('欢迎使用MoBlog！','<p>欢迎使用MoBlog！</p><p><strong>我的目标：简洁，易用，高质</strong>！！！</p><p>&nbsp;</p><table style=""border-collapse:collapse;"" class=""ke-zeroborder"" border=""0"" cellspacing=""0"" cellpadding=""4"" width=""100%""><tbody><tr><td height=""28"" width=""100"" colspan=""2"" align=""left"">帮助文档：<a href=""http://www.9fn.net/help/"" target=""_blank"">http://www.9fn.net/help/</a> </td></tr><tr><td height=""28"" width=""100"" colspan=""2"" align=""left"">更新日志：<a href=""http://www.9fn.net/update.html"" target=""_blank"">http://www.9fn.net/update.html</a> </td></tr><tr><td height=""28"" width=""100"" colspan=""2"" align=""left"">QQ群：127430216（MoBlog内部交流）-依托于MoAspEnginer，本框架是群讨论重点；本群非ASP答疑群，见谅。 </td></tr></tbody></table>','原创',2,'','2013年4月','首次使用','1,2')"
				else
					ec = ec & "<span class=""red"">数据表[Diary]创建失败!</span><br />"
					ef = true
				end if
			end if
			if not ef then	
				'创建表：Guestbook
				Set Fields = ADOX.CreateFieldsCollection()
				Fields.Append ADOX.CreateField("id").int().primarykey().IDENTITY(true)
				Fields.Append ADOX.CreateField("name").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("content").text().nullable(true)
				Fields.Append ADOX.CreateField("ly_date").datetime("getdate()").nullable(true)
				Fields.Append ADOX.CreateField("ly_ip").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("email").VARCHAR().length(100).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("rely").text().nullable(true)
				Fields.Append ADOX.CreateField("redate").datetime().nullable(true)
				if f.post("dbtype")="access" then
					Fields.Append ADOX.CreateField("secret").VARCHAR("""no""").length(10).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				elseif f.post("dbtype")="mssql" then
					Fields.Append ADOX.CreateField("secret").VARCHAR("'no'").length(10).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				end if
				Fields.Append ADOX.CreateField("forarticle").int(0).nullable(true)
				Fields.Append ADOX.CreateField("pid").int(0).nullable(true)
				Fields.Append ADOX.CreateField("isreplay").int(0).nullable(true)
				Fields.Append ADOX.CreateField("homepage").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("replayid").int(0).nullable(true)
				Fields.Append ADOX.CreateField("checked").int(1).nullable(true)
				if ADOX.CreateTable("" & prex & "Guestbook",Fields,true) then
					ec = ec & "数据表[Guestbook]创建成功!<br />"
				else
					ec = ec & "<span class=""red"">数据表[Guestbook]创建失败!</span><br />"
					ef = true
				end if
			end if
			if not ef then		
				'创建表：Links
				Set Fields = ADOX.CreateFieldsCollection()
				Fields.Append ADOX.CreateField("id").int().primarykey().IDENTITY(true)
				Fields.Append ADOX.CreateField("title").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("url").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("image_").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("author").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("ischecked").int(0).nullable(true)
				Fields.Append ADOX.CreateField("link_order").int(0).nullable(true)
				Fields.Append ADOX.CreateField("isimage").int(0).nullable(true)
				Fields.Append ADOX.CreateField("clickcount").int(0).nullable(true)
				if ADOX.CreateTable("" & prex & "Links",Fields,true) then
					ec = ec & "数据表[Links]创建成功!<br />"
				else
					ec = ec & "<span class=""red"">数据表[Links]创建失败!</span><br />"
					ef=true
				end if
			end if
			if not ef then		
				'创建表：Log
				Set Fields = ADOX.CreateFieldsCollection()
				Fields.Append ADOX.CreateField("id").int().primarykey().IDENTITY(true)
				Fields.Append ADOX.CreateField("username").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("logdate").datetime("getdate()").nullable(true)
				Fields.Append ADOX.CreateField("model").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("action").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("description").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("ip").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("status").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				if ADOX.CreateTable("" & prex & "Log",Fields,true) then
					ec = ec & "数据表[Log]创建成功!<br />"
				else
					ec = ec & "<span class=""red"">数据表[Log]创建失败!</span><br />"
					ef=true
				end if
			end if
			if not ef then		
				'创建表：Ping
				Set Fields = ADOX.CreateFieldsCollection()
				Fields.Append ADOX.CreateField("id").int().primarykey().IDENTITY(true)
				Fields.Append ADOX.CreateField("name").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("url").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				if f.post("dbtype")="access" then
					Fields.Append ADOX.CreateField("enabled").VARCHAR("""yes""").length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				elseif f.post("dbtype")="mssql" then
					Fields.Append ADOX.CreateField("enabled").VARCHAR("'yes'").length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				end if
				if ADOX.CreateTable("" & prex & "Ping",Fields,true) then
					ec = ec & "数据表[Ping]创建成功!<br />"
					ADOX.Exec "insert into " & prex & "Ping(name,url,enabled) values('yodao','http://blog.yodao.com/ping/RPC2;weblogUpdates.extendedPing','yes')"
					ADOX.Exec "insert into " & prex & "Ping(name,url,enabled) values('baidu','http://ping.baidu.com/ping/RPC2;weblogUpdates.extendedPing','yes')"
					ADOX.Exec "insert into " & prex & "Ping(name,url,enabled) values('google','http://blogsearch.google.com/ping/RPC2;weblogUpdates.extendedPing','yes')"
					ADOX.Exec "insert into " & prex & "Ping(name,url,enabled) values('weblogs','http://rpc.weblogs.com/RPC2;weblogUpdates.ping','yes')"
					ADOX.Exec "insert into " & prex & "Ping(name,url,enabled) values('pingomatic','http://rpc.pingomatic.com/','yes')"
					ADOX.Exec "insert into " & prex & "Ping(name,url,enabled) values('feedsky','http://www.feedsky.com/api/RPC2','no')"
					ADOX.Exec "insert into " & prex & "Ping(name,url,enabled) values('feedburner','http://ping.feedburner.com','no')"
				else
					ec = ec & "<span class=""red"">数据表[Ping]创建失败!</span><br />"
					ef=true
				end if
			end if
			if not ef then		
				'创建表：System
				Set Fields = ADOX.CreateFieldsCollection()
				Fields.Append ADOX.CreateField("ID").int().primarykey().IDENTITY(true)
				Fields.Append ADOX.CreateField("title").varchar().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_hostname").varchar().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_title").varchar().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_description").varchar().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_keyword").varchar().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_pagesize").int(0).nullable(true)
				Fields.Append ADOX.CreateField("Master_Name").varchar().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("Master_NickName").varchar().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("Master_Sex").varchar().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("Master_Birthday").varchar().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("Master_Star").varchar().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("Master_blood").varchar().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("Master_selfIntro").text().COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_sub_title").varchar().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_allow_comments").varchar().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_allow_guestbook").varchar().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_state_closed").varchar().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_allow_apply_links").varchar().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_state_closed_reason").varchar().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_copy_right").varchar().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_email").varchar().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_mail_server").varchar().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_mail_username").varchar().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_mail_password").varchar().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_mail_display").varchar().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_allow_mail_send").varchar().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_mail_sender").varchar().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_ping_opened").varchar().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_template").varchar().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_rss_format").varchar().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_block_mode").varchar().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_block_black").varchar().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_block_white").varchar().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_comments_check").varchar().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_visit_timeout").int(60).nullable(true)
				
				if ADOX.CreateTable("" & prex & "System",Fields,true) then
					ec = ec & "数据表[System]创建成功!<br />"
					ADOX.Exec "insert into " & prex & "System(title,blog_hostname,blog_title,blog_description,blog_keyword,blog_pagesize,Master_Name,Master_NickName,Master_Sex,Master_Birthday,Master_Star,Master_blood,Master_selfIntro,blog_sub_title,blog_allow_comments,blog_allow_guestbook,blog_state_closed,blog_allow_apply_links,blog_state_closed_reason,blog_copy_right,blog_email,blog_mail_server,blog_mail_username,blog_mail_password,blog_mail_display,blog_allow_mail_send,blog_mail_sender,blog_ping_opened,blog_template,blog_rss_format,blog_block_mode,blog_block_black,blog_block_white,blog_comments_check,blog_visit_timeout) values('MoBlog：开动大脑，一切都是浮云---','http://localhost','MoBlog','MoBlog：开动大脑，一切都是浮云---','MoBlog',0,'','','','','','','MoBlog','开动大脑，一切都是浮云---','yes','yes','no','yes','','copyright&copy;2013 localhost All Rights Reserved.','XXXX@XX.com','smtp.xx.com','XXX@xx.com','XXXXXX','MoBlog','no','XXX@xx.com','yes','default','dynamic','none','127.0.0.1','127.0.0.1','no',60)"
				else
					ec = ec & "<span class=""red"">数据表[System]创建失败!</span><br />"
					ef=true
				end if
			end if
			if not ef then			
				'创建表：Tags
				Set Fields = ADOX.CreateFieldsCollection()
				Fields.Append ADOX.CreateField("id").int().primarykey().IDENTITY(true)
				Fields.Append ADOX.CreateField("name").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("tag").VARCHAR().length(50).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("counts").int(0).nullable(true)
				if ADOX.CreateTable("" & prex & "Tags",Fields,true) then
					ec = ec & "数据表[Tags]创建成功!<br />"
				else
					ec = ec & "<span class=""red"">数据表[Tags]创建失败!</span><br />"
					ef=true
				end if
			end if
			if not ef then			
				'创建表：Trackbacks
				Set Fields = ADOX.CreateFieldsCollection()
				Fields.Append ADOX.CreateField("id").int().primarykey().IDENTITY(true)
				Fields.Append ADOX.CreateField("title").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("excerpt").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("url").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("blog_name").VARCHAR().length(255).COLLATE("Chinese_PRC_CI_AS").nullable(true)
				Fields.Append ADOX.CreateField("tbdate").datetime("getdate()").nullable(true)
				Fields.Append ADOX.CreateField("artid").int(0).nullable(true)
				Fields.Append ADOX.CreateField("canshow").int(0).nullable(true)
				if ADOX.CreateTable("" & prex & "Trackbacks",Fields,true) then
					ec = ec & "数据表[Trackbacks]创建成功!<br />"
				else
					ec = ec & "<span class=""red"">数据表[Trackbacks]创建失败!</span><br />"	
					ef=true
				end if
			end if 
			if not ef then
				Dim File:Set File = Mo.Static("File")
				Dim Text:Text = File.readtext("App/Config/Config.asp")
				if Text<>"" then
					Text = ReplaceEx(Text,"MO_TABLE_PERX \= ""(.*?)""","MO_TABLE_PERX = """ & f.post("prex") & """")
					File.writetext "App/Config/Config.asp",Text
					Text = ReplaceEx(Text,"MO_TAG_LIB \= ""(.*?)""","MO_TAG_LIB = """"")
					File.writetext "Install/Config/Config.asp",Text
					Text = File.readtext("App/Config/Mo.Conf.DB.asp")
					if Text<>"" then
						Text = ReplaceEx(Text,"DB_Type\:""(.*?)""","DB_Type:""" & ucase(f.post("dbtype")) & """")
						if f.post("dbtype")="access" then
							Text = ReplaceEx(Text,"DB_Path\:""(.*?)""","DB_Path:""" & (f.post.Safe("path")) & """")
							Text = ReplaceEx(Text,"DB_Server\:""(.*?)""","DB_Server:""""")
							Text = ReplaceEx(Text,"DB_Username\:""(.*?)""","DB_Username:""""")
							Text = ReplaceEx(Text,"DB_Password\:""(.*?)""","DB_Password:""""")
							Text = ReplaceEx(Text,"DB_Name\:""(.*?)""","DB_Name:""""")
						else
							Text = ReplaceEx(Text,"DB_Path\:""(.*?)""","DB_Path:""""")
							Text = ReplaceEx(Text,"DB_Server\:""(.*?)""","DB_Server:""" & (f.post.Safe("server")) & """")
							Text = ReplaceEx(Text,"DB_Username\:""(.*?)""","DB_Username:""" & (f.post.Safe("username")) & """")
							Text = ReplaceEx(Text,"DB_Password\:""(.*?)""","DB_Password:""" & (f.post.Safe("password")) & """")
							Text = ReplaceEx(Text,"DB_Name\:""(.*?)""","DB_Name:""" & (f.post.Safe("dbname")) & """")
						end if
						File.writetext "App/Config/Mo.Conf.DB.asp",Text
						File.writetext "install.lock",F.formatdate(now,"yyyy-MM-dd HH:mm:ss")
						File.writetext "Install/Config/Mo.Conf.DB.asp",Text
					else
						ef = true
						ec = ec & "数据库配置文件读取失败！"
					end if
				else
					ef = true
					ec = ec & "站点配置文件读取失败！"
				end if
			end if
		end if
		if not ef then
			F.echo "数据库配置成功"
		else
			F.echo ec
		end if
	end sub
end class
%>