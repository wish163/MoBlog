/**
 * SyntaxHighlighter
 * http://alexgorbatchev.com/SyntaxHighlighter
 *
 * SyntaxHighlighter is donationware. If you are using it, please donate.
 * http://alexgorbatchev.com/SyntaxHighlighter/donate.html
 *
 * @version
 * 3.0.83 (July 02 2010)
 * 
 * @copyright
 * Copyright (C) 2004-2010 Alex Gorbatchev.
 *
 * @license
 * Dual licensed under the MIT and GPL licenses.
 * @modify by
 * anlige @ 2013-11-15
 */
(function() {
	var sh = SyntaxHighlighter;
	function _getBasePath() {
		var els = document.getElementsByTagName('script'), src;
		for (var i = 0, len = els.length; i < len; i++) {
			src = els[i].src || '';
			if (/shCore(.*?)\.js/.test(src)) {
				return src.substring(0, src.lastIndexOf('/') + 1);
			}
		}
		return '';
	}
	sh.basePath=_getBasePath();
	sh.brushesConfig_=[
		['applescript','AppleScript'],
		['actionscript3','as3','AS3'],
		['bash','shell','Bash'],
		['coldfusion','cf','ColdFusion'],
		['cpp','c','Cpp'],
		['c#','c-sharp','csharp','CSharp'],
		['css','CSS'],
		['delphi', 'pascal', 'pas','Delphi'],
		['diff', 'patch','Diff'],
		['erl', 'erlang','Erland'],
		['groovy','Groovy'],
		['java','Java'],
		['jfx', 'javafx','JavaFX'],
		['js', 'jscript', 'javascript','JScript'],
		['perl', 'Perl', 'pl','Perl'],
		['php','Php'],
		['text', 'plain','Plain'],
		['powershell', 'ps','PowerShell'],
		['py', 'python','Python'],
		['ruby', 'rails', 'ror', 'rb','Ruby'],
		['sass', 'scss','Sass'],
		['scala','Scala'],
		['sql','Sql'],
		['vb', 'vbnet','vbs','vbscript','Vb'],
		['xml', 'xhtml', 'xslt', 'html','Xml']
	];
	sh.brushesConfig={};
	for(var i=0;i<sh.brushesConfig_.length;i++){
		var conf = sh.brushesConfig_[i];
		var url = conf.pop();
		for(var j=0;j<conf.length;j++){
			sh.brushesConfig[conf[j]]=url;
		}
	}
	sh.autoloader = function()
	{
		var list = arguments,
			elements = sh.findElements(),
			brushes = {},
			scripts = {},
			all = SyntaxHighlighter.all,
			allCalled = false,
			allParams = null,
			i
			;
			
		SyntaxHighlighter.all = function(params)
		{
			allParams = params;
			allCalled = true;
		};
		
		for (i = 0; i < elements.length; i++)
		{
			var url = sh.brushesConfig[elements[i].params.brush];
			
			if (!url)
				continue;
			url = sh.basePath + "shBrush"+url+".js";
			scripts[url] = false;
			loadScript(url);
		}
		function loadScript(url)
		{
			var script = document.createElement('script'),
				done = false
				;
			script.src = url;
			script.type = 'text/javascript';
			script.language = 'javascript';
			script.onload = script.onreadystatechange = function()
			{
				if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete'))
				{
					done = true;
					scripts[url] = true;
					checkAll();
					script.onload = script.onreadystatechange = null;
					script.parentNode.removeChild(script);
				}
			};
			document.body.appendChild(script);
		};
		
		function checkAll()
		{
			for(var url in scripts)
				if (scripts[url] == false)
					return;
			if (allCalled)
				SyntaxHighlighter.highlight(allParams);
		};
		return SyntaxHighlighter;
	};
})();
